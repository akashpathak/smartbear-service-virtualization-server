# Service Virtualization Server

SmartBear service virtualization server allows users to build API virtualizations to virtualize their API/service. To communicate with our minimalistic server we have defined couple of REST APIs.

##### Way to run:

1.  yarn
2.  yarn dev

##### For changing Server host

Visit ./helper/shared/constants.js

##### For test

I tried integrating test case, but by Nextjs it was taking more time then expected. So, apology for not including test cases.
