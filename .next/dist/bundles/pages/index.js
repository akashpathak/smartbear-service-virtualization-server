module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/_board/board-page.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Board;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__card__ = __webpack_require__("./components/_card/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__empty_board__ = __webpack_require__("./components/_empty-board/index.js");
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/components/_board/board-page.js";


//Components


function Board(props) {
  return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    className: "jsx-3661109929"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("ul", {
    id: "board",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    className: "jsx-3661109929"
  }, props.getVirtualDataObject.length <= 0 ? __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__empty_board__["a" /* default */], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }) : props.getVirtualDataObject.map(function (virtualData) {
    return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("li", {
      key: virtualData.virtualizationID,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 13
      },
      className: "jsx-3661109929"
    }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__card__["a" /* default */], {
      id: virtualData.virtualizationID,
      port: virtualData.port,
      title: virtualData.name,
      apiType: virtualData.apiType,
      running: virtualData.running,
      protocol: virtualData.protocol,
      isRunningToggle: props.isRunningToggle,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14
      }
    }));
  })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
    styleId: "3661109929",
    css: "#board.jsx-3661109929{list-style:none;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-flow:row wrap;-ms-flex-flow:row wrap;flex-flow:row wrap;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;-webkit-align-items:start;-webkit-box-align:start;-ms-flex-align:start;align-items:start;height:auto;padding:30px 0;margin:0;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvX2JvYXJkL2JvYXJkLXBhZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBMEJrQixBQUcyQixnQkFDSCwwRUFDTSxxRUFDSSxtR0FDTCx5RkFDTixZQUNHLGVBQ04sU0FDWCIsImZpbGUiOiJjb21wb25lbnRzL19ib2FyZC9ib2FyZC1wYWdlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9ha2FzaHBhdGhhay9Qcm9qZWN0cy9zbWFydGJlYXIiLCJzb3VyY2VzQ29udGVudCI6WyIvL0NvbXBvbmVudHNcbmltcG9ydCBDYXJkcyBmcm9tIFwiLi4vX2NhcmRcIjtcbmltcG9ydCBFbXB0eUJvYXJkIGZyb20gXCIuLi9fZW1wdHktYm9hcmRcIjtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQm9hcmQocHJvcHMpIHtcbiAgcmV0dXJuIChcbiAgICA8ZGl2PlxuICAgICAgPHVsIGlkPVwiYm9hcmRcIj5cbiAgICAgICAge3Byb3BzLmdldFZpcnR1YWxEYXRhT2JqZWN0Lmxlbmd0aCA8PSAwID8gKFxuICAgICAgICAgIDxFbXB0eUJvYXJkIC8+XG4gICAgICAgICkgOiAoXG4gICAgICAgICAgcHJvcHMuZ2V0VmlydHVhbERhdGFPYmplY3QubWFwKHZpcnR1YWxEYXRhID0+IChcbiAgICAgICAgICAgIDxsaSBrZXk9e3ZpcnR1YWxEYXRhLnZpcnR1YWxpemF0aW9uSUR9PlxuICAgICAgICAgICAgICA8Q2FyZHNcbiAgICAgICAgICAgICAgICBpZD17dmlydHVhbERhdGEudmlydHVhbGl6YXRpb25JRH1cbiAgICAgICAgICAgICAgICBwb3J0PXt2aXJ0dWFsRGF0YS5wb3J0fVxuICAgICAgICAgICAgICAgIHRpdGxlPXt2aXJ0dWFsRGF0YS5uYW1lfVxuICAgICAgICAgICAgICAgIGFwaVR5cGU9e3ZpcnR1YWxEYXRhLmFwaVR5cGV9XG4gICAgICAgICAgICAgICAgcnVubmluZz17dmlydHVhbERhdGEucnVubmluZ31cbiAgICAgICAgICAgICAgICBwcm90b2NvbD17dmlydHVhbERhdGEucHJvdG9jb2x9XG4gICAgICAgICAgICAgICAgaXNSdW5uaW5nVG9nZ2xlPXtwcm9wcy5pc1J1bm5pbmdUb2dnbGV9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICkpXG4gICAgICAgICl9XG4gICAgICA8L3VsPlxuICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAjYm9hcmQge1xuICAgICAgICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBmbGV4LWZsb3c6IHJvdyB3cmFwO1xuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICAgIGFsaWduLWl0ZW1zOiBzdGFydDtcbiAgICAgICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAgICAgcGFkZGluZzogMzBweCAwO1xuICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgfVxuICAgICAgYH08L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufVxuIl19 */\n/*@ sourceURL=components/_board/board-page.js */"
  }));
}

/***/ }),

/***/ "./components/_board/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__board_page__ = __webpack_require__("./components/_board/board-page.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__board_page__["a"]; });


/***/ }),

/***/ "./components/_card/card-page.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal__ = __webpack_require__("./components/_modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__top_bar__ = __webpack_require__("./components/_top-bar/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__protocol__ = __webpack_require__("./components/_protocol/index.js");
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/components/_card/card-page.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

 //Component





var Card =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Card, _React$Component);

  function Card(props) {
    var _this;

    _classCallCheck(this, Card);

    _this = _possibleConstructorReturn(this, (Card.__proto__ || Object.getPrototypeOf(Card)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "handleModal", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(isOpen) {
        _this.setState({
          editCard: isOpen
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "handleOpen", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.handleModal(true);
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "handleInputChange", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(e) {
        var _e$target = e.target,
            name = _e$target.name,
            value = _e$target.value;

        _this.setState({
          dirtyFields: _objectSpread({}, _this.state.dirtyFields, _defineProperty({}, name, value))
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "handleSubmit", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(event, id) {
        event.preventDefault(); // We need to make api call from here

        var postingData = {};

        if (_this.state.dirtyFields.title !== _this.props.title) {
          postingData["name"] = _this.state.dirtyFields.title;
        } else if (_this.state.dirtyFields.port !== _this.props.port) {
          postingData["port"] = _this.state.dirtyFields.port;
        } else if (_this.state.dirtyFields.protocol !== _this.props.protocol) {
          postingData["protocol"] = _this.state.dirtyFields.protocol;
        } // Checking if putData is empty object then dont make a call


        if (Object.keys(postingData).length === 0 && postingData.constructor === Object) {
          _this.handleModal(false);
        } else {
          _this.props.isRunningToggle(id, postingData).then(function (res) {
            return res === "OK" && _this.handleModal(false);
          });
        }
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "handleStartStop", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value(event, id, kind) {
        event.preventDefault();
        console.log(kind);
        var StatusObject = {
          running: !_this.state.running
        };

        _this.props.isRunningToggle(id, StatusObject).then(function (res) {
          return res === "OK" && _this.setState({
            running: !kind
          });
        });
      }
    });
    Object.defineProperty(_assertThisInitialized(_this), "handleClose", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function value() {
        _this.handleModal(false);
      }
    });
    _this.state = {
      running: props.running,
      editCard: '',
      dirtyFields: {
        port: props.port,
        protocol: props.protocol,
        title: props.title
      }
    };
    return _this;
  }

  _createClass(Card, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        className: "jsx-3324869512" + " " + "card"
      }, this.state.editCard && __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__modal__["a" /* default */], {
        onModalToggle: this.handleModal,
        modalTitle: "Edit this service",
        id: this.props.id,
        dirtyFields: this.state.dirtyFields,
        handleClose: this.handleClose,
        handleSubmit: this.handleSubmit,
        handleInputChange: this.handleInputChange,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 91
        }
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__top_bar__["a" /* default */], {
        id: this.props.id,
        port: this.props.port,
        title: this.props.title,
        onEditButtonClick: this.handleOpen,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        }
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("h2", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        className: "jsx-3324869512" + " " + "title"
      }, this.props.title), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        className: "jsx-3324869512" + " " + "description"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        className: "jsx-3324869512"
      }, this.props.apiType), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("button", {
        onClick: function onClick(event) {
          return _this2.handleStartStop(event, _this2.props.id, _this2.state.running);
        },
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        className: "jsx-3324869512" + " " + ((this.state.running ? "play" : "play active") || "")
      })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__protocol__["a" /* default */], {
        protocol: this.props.protocol,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        }
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "3324869512",
        css: ".card.jsx-3324869512{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;text-align:left;-webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;height:auto;width:33rem;max-width:100vw;-webkit-flex-basis:auto;-ms-flex-preferred-size:auto;flex-basis:auto;-webkit-box-flex:1;-webkit-flex-grow:1;-ms-flex-positive:1;flex-grow:1;margin:20px;background-color:#fafafa;box-shadow:0 0.5rem 3rem rgba(0,0,0,0.1);}.card.jsx-3324869512 .title.jsx-3324869512{color:#212121;font-size:18px;padding:calc(10px / 2);text-transform:uppercase;margin:0;color:#fff;background-color:#4066c8;}.card.jsx-3324869512 .description.jsx-3324869512{text-align:top;min-height:120px;}.card.jsx-3324869512 .description.jsx-3324869512 p.jsx-3324869512{color:#212121;font-size:16px;padding:calc(10px / 2);margin:0;font-weight:bold;border-left:5px solid #4066c8;}.play.jsx-3324869512{cursor:pointer;display:inline-block;padding:0.75rem;width:100%;background:transparent;border:0;text-align:left;width:20%;}.play.jsx-3324869512:before,.play.jsx-3324869512:after{content:\"\";display:inline-block;vertical-align:middle;-webkit-transition:all 0.75s;transition:all 0.75s;}.play.jsx-3324869512:before{height:40px;border:20px solid #9c0;border-right:none;border-top:13px solid transparent;border-bottom:13px solid transparent;}.play.jsx-3324869512:after{margin:5px 0;height:0px;border:30px solid #9c0;border-right:none;border-top:20px solid transparent;border-bottom:20px solid transparent;}.play.active.jsx-3324869512:before{border-top-width:0px;border-bottom-width:0px;height:60px;border-color:#dc3522;}.play.active.jsx-3324869512:after{height:60px;border-top-width:0px;border-bottom-width:0px;border-left-width:20px;margin-left:10px;border-color:#dc3522;}.play.jsx-3324869512:focus{outline:none;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvX2NhcmQvY2FyZC1wYWdlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXNIVyxBQUc0QixBQWlCQyxBQVNDLEFBSUQsQUFRQyxBQVdKLEFBTUMsQUFPQyxBQVFRLEFBTVQsQUFRQyxXQWxDUSxDQU1FLEFBcUJGLENBZFYsQUFzQmIsQ0FuRWlCLEFBYUEsQ0FKRSxBQVlJLE1BZ0NHLEdBUEQsS0E3Q0EsQUFhQSxHQUp6QixBQXVCd0IsQ0EyQkUsRUFyQk4sQ0FqQkYsU0FnQ0osRUFQTSxLQTdDTyxBQWFoQixBQVFFLENBaUJ1QixDQU5iLEdBcUJBLEFBTUUsSUE3Q04sRUFRTSxFQXdCVyxTQWpFbEIsR0FvQlAsQ0FjcUIsQUF1Q2hDLEVBTW1CLE1BMUROLEFBcUJGLENBZ0I0QixHQXpEZixLQTBDTixFQXJCUyxBQTBESixFQWRnQixLQWR2QyxJQWpCQSxHQVNZLE9BcUNaLEdBcENBLENBdEJBLEVBb0NBLFlBUUEsZ0NBakVnQyxtSEFDbEIsWUFDQSxZQUNJLGdCQUNBLHFFQUNKLHVFQUNBLFlBQ2EseUJBQ21CLHlDQUk5QyIsImZpbGUiOiJjb21wb25lbnRzL19jYXJkL2NhcmQtcGFnZS5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWthc2hwYXRoYWsvUHJvamVjdHMvc21hcnRiZWFyIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gXCJyZWFjdFwiO1xuLy9Db21wb25lbnRcbmltcG9ydCBNb2RhbFBhZ2UgZnJvbSBcIi4uL19tb2RhbFwiO1xuaW1wb3J0IFRvcEJhciBmcm9tIFwiLi4vX3RvcC1iYXJcIjtcbmltcG9ydCBQcm90b2NvbCBmcm9tIFwiLi4vX3Byb3RvY29sXCI7XG5cbmNsYXNzIENhcmQgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgcnVubmluZzogcHJvcHMucnVubmluZyxcbiAgICAgIGVkaXRDYXJkOicnLFxuICAgICAgZGlydHlGaWVsZHM6IHtcbiAgICAgICAgcG9ydDogcHJvcHMucG9ydCxcbiAgICAgICAgcHJvdG9jb2w6IHByb3BzLnByb3RvY29sLFxuICAgICAgICB0aXRsZTogcHJvcHMudGl0bGVcbiAgICAgIH1cbiAgICB9O1xuICB9XG5cbiAgaGFuZGxlTW9kYWwgPSAoaXNPcGVuKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXRDYXJkOiBpc09wZW4gfSk7XG4gIH07XG5cbiAgLy8gT3BlbiBtb2RhbFxuICBoYW5kbGVPcGVuID0gKCkgPT4ge1xuICAgIHRoaXMuaGFuZGxlTW9kYWwodHJ1ZSk7XG4gIH07XG5cbiAgLy8gRWRpdCBtb2RhbCBmaWVsZHNcbiAgaGFuZGxlSW5wdXRDaGFuZ2UgPSBlID0+IHtcbiAgICBjb25zdCB7IG5hbWUsIHZhbHVlIH0gPSBlLnRhcmdldDtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGRpcnR5RmllbGRzOiB7XG4gICAgICAgIC4uLnRoaXMuc3RhdGUuZGlydHlGaWVsZHMsXG4gICAgICAgIFtuYW1lXTogdmFsdWVcbiAgICAgIH1cbiAgICB9KTtcbiAgfTtcblxuICAvLyBIYW5kbGUgZm9ybSBzdWJtaXRcbiAgaGFuZGxlU3VibWl0ID0gKGV2ZW50LCBpZCkgPT4ge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgLy8gV2UgbmVlZCB0byBtYWtlIGFwaSBjYWxsIGZyb20gaGVyZVxuICAgIGxldCBwb3N0aW5nRGF0YSA9IHt9O1xuICAgIGlmICh0aGlzLnN0YXRlLmRpcnR5RmllbGRzLnRpdGxlICE9PSB0aGlzLnByb3BzLnRpdGxlKSB7XG4gICAgICBwb3N0aW5nRGF0YVtcIm5hbWVcIl0gPSB0aGlzLnN0YXRlLmRpcnR5RmllbGRzLnRpdGxlO1xuICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0ZS5kaXJ0eUZpZWxkcy5wb3J0ICE9PSB0aGlzLnByb3BzLnBvcnQpIHtcbiAgICAgIHBvc3RpbmdEYXRhW1wicG9ydFwiXSA9IHRoaXMuc3RhdGUuZGlydHlGaWVsZHMucG9ydDtcbiAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdGUuZGlydHlGaWVsZHMucHJvdG9jb2wgIT09IHRoaXMucHJvcHMucHJvdG9jb2wpIHtcbiAgICAgIHBvc3RpbmdEYXRhW1wicHJvdG9jb2xcIl0gPSB0aGlzLnN0YXRlLmRpcnR5RmllbGRzLnByb3RvY29sO1xuICAgIH1cbiAgICAvLyBDaGVja2luZyBpZiBwdXREYXRhIGlzIGVtcHR5IG9iamVjdCB0aGVuIGRvbnQgbWFrZSBhIGNhbGxcbiAgICBpZiAoXG4gICAgICBPYmplY3Qua2V5cyhwb3N0aW5nRGF0YSkubGVuZ3RoID09PSAwICYmXG4gICAgICBwb3N0aW5nRGF0YS5jb25zdHJ1Y3RvciA9PT0gT2JqZWN0XG4gICAgKSB7XG4gICAgICB0aGlzLmhhbmRsZU1vZGFsKGZhbHNlKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5wcm9wc1xuICAgICAgICAuaXNSdW5uaW5nVG9nZ2xlKGlkLCBwb3N0aW5nRGF0YSlcbiAgICAgICAgLnRoZW4ocmVzID0+IHJlcyA9PT0gXCJPS1wiICYmIHRoaXMuaGFuZGxlTW9kYWwoZmFsc2UpKTtcbiAgICB9XG4gIH07XG5cbiAgLy8gSGFuZGxlIFN0YXJ0L1N0b3Agc2VydmljZVxuICBoYW5kbGVTdGFydFN0b3AgPSAoZXZlbnQsIGlkLCBraW5kKSA9PiB7XG4gICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zb2xlLmxvZyhraW5kKTtcbiAgICBsZXQgU3RhdHVzT2JqZWN0ID0ge1xuICAgICAgcnVubmluZzogIXRoaXMuc3RhdGUucnVubmluZ1xuICAgIH07XG4gICAgdGhpcy5wcm9wcy5pc1J1bm5pbmdUb2dnbGUoaWQsIFN0YXR1c09iamVjdCkudGhlbihcbiAgICAgIHJlcyA9PlxuICAgICAgICByZXMgPT09IFwiT0tcIiAmJlxuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBydW5uaW5nOiAha2luZFxuICAgICAgICB9KVxuICAgICk7XG4gIH07XG5cbiAgLy8gSGFuZGxlIE1vZGFsIGNsb3NlXG4gIGhhbmRsZUNsb3NlID0gKCkgPT4ge1xuICAgIHRoaXMuaGFuZGxlTW9kYWwoZmFsc2UpO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjYXJkXCI+XG4gICAgICAgIHt0aGlzLnN0YXRlLmVkaXRDYXJkICYmIChcbiAgICAgICAgICA8TW9kYWxQYWdlXG4gICAgICAgICAgICBvbk1vZGFsVG9nZ2xlPXt0aGlzLmhhbmRsZU1vZGFsfVxuICAgICAgICAgICAgbW9kYWxUaXRsZT1cIkVkaXQgdGhpcyBzZXJ2aWNlXCJcbiAgICAgICAgICAgIGlkPXt0aGlzLnByb3BzLmlkfVxuICAgICAgICAgICAgZGlydHlGaWVsZHM9e3RoaXMuc3RhdGUuZGlydHlGaWVsZHN9XG4gICAgICAgICAgICBoYW5kbGVDbG9zZT17dGhpcy5oYW5kbGVDbG9zZX1cbiAgICAgICAgICAgIGhhbmRsZVN1Ym1pdD17dGhpcy5oYW5kbGVTdWJtaXR9XG4gICAgICAgICAgICBoYW5kbGVJbnB1dENoYW5nZT17dGhpcy5oYW5kbGVJbnB1dENoYW5nZX1cbiAgICAgICAgICAvPlxuICAgICAgICApfVxuICAgICAgICA8VG9wQmFyXG4gICAgICAgICAgaWQ9e3RoaXMucHJvcHMuaWR9XG4gICAgICAgICAgcG9ydD17dGhpcy5wcm9wcy5wb3J0fVxuICAgICAgICAgIHRpdGxlPXt0aGlzLnByb3BzLnRpdGxlfVxuICAgICAgICAgIG9uRWRpdEJ1dHRvbkNsaWNrPXt0aGlzLmhhbmRsZU9wZW59XG4gICAgICAgIC8+XG4gICAgICAgIDxoMiBjbGFzc05hbWU9XCJ0aXRsZVwiPnt0aGlzLnByb3BzLnRpdGxlfTwvaDI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZGVzY3JpcHRpb25cIj5cbiAgICAgICAgICA8cD57dGhpcy5wcm9wcy5hcGlUeXBlfTwvcD5cbiAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICBjbGFzc05hbWU9e3RoaXMuc3RhdGUucnVubmluZyA/IFwicGxheVwiIDogXCJwbGF5IGFjdGl2ZVwifVxuICAgICAgICAgICAgb25DbGljaz17ZXZlbnQgPT5cbiAgICAgICAgICAgICAgdGhpcy5oYW5kbGVTdGFydFN0b3AoZXZlbnQsIHRoaXMucHJvcHMuaWQsIHRoaXMuc3RhdGUucnVubmluZylcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPFByb3RvY29sIHByb3RvY29sPXt0aGlzLnByb3BzLnByb3RvY29sfSAvPlxuICAgICAgICA8c3R5bGUganN4PlxuICAgICAgICAgIHtgXG4gICAgICAgICAgICAuY2FyZCB7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgICAgICAgaGVpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgICB3aWR0aDogMzNyZW07XG4gICAgICAgICAgICAgIG1heC13aWR0aDogMTAwdnc7XG4gICAgICAgICAgICAgIGZsZXgtYmFzaXM6IGF1dG87IC8qIGRlZmF1bHQgdmFsdWUgKi9cbiAgICAgICAgICAgICAgZmxleC1ncm93OiAxO1xuICAgICAgICAgICAgICBtYXJnaW46IDIwcHg7XG4gICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmYWZhZmE7XG4gICAgICAgICAgICAgIGJveC1zaGFkb3c6IDAgMC41cmVtIDNyZW0gcmdiYSgwLCAwLCAwLCAwLjEpO1xuICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIC8qIGJvcmRlci1yYWRpdXM6IDAuN3JlbTsgKi9cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNhcmQgLnRpdGxlIHtcbiAgICAgICAgICAgICAgY29sb3I6ICMyMTIxMjE7XG4gICAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgICAgcGFkZGluZzogY2FsYygxMHB4IC8gMik7XG4gICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM0MDY2Yzg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAuY2FyZCAuZGVzY3JpcHRpb24ge1xuICAgICAgICAgICAgICB0ZXh0LWFsaWduOiB0b3A7XG4gICAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDEyMHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLmNhcmQgLmRlc2NyaXB0aW9uIHAge1xuICAgICAgICAgICAgICBjb2xvcjogIzIxMjEyMTtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgICBwYWRkaW5nOiBjYWxjKDEwcHggLyAyKTtcbiAgICAgICAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgICAgICAgYm9yZGVyLWxlZnQ6IDVweCBzb2xpZCAjNDA2NmM4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnBsYXkge1xuICAgICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgcGFkZGluZzogMC43NXJlbTtcbiAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgICAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgICBib3JkZXI6IDA7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgICAgICAgIHdpZHRoOiAyMCU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAucGxheTpiZWZvcmUsXG4gICAgICAgICAgICAucGxheTphZnRlciB7XG4gICAgICAgICAgICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuNzVzO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnBsYXk6YmVmb3JlIHtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICAgICAgICBib3JkZXI6IDIwcHggc29saWQgIzljMDtcbiAgICAgICAgICAgICAgYm9yZGVyLXJpZ2h0OiBub25lO1xuICAgICAgICAgICAgICBib3JkZXItdG9wOiAxM3B4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAxM3B4IHNvbGlkIHRyYW5zcGFyZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLnBsYXk6YWZ0ZXIge1xuICAgICAgICAgICAgICBtYXJnaW46IDVweCAwO1xuICAgICAgICAgICAgICBoZWlnaHQ6IDBweDtcbiAgICAgICAgICAgICAgYm9yZGVyOiAzMHB4IHNvbGlkICM5YzA7XG4gICAgICAgICAgICAgIGJvcmRlci1yaWdodDogbm9uZTtcbiAgICAgICAgICAgICAgYm9yZGVyLXRvcDogMjBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMjBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5wbGF5LmFjdGl2ZTpiZWZvcmUge1xuICAgICAgICAgICAgICBib3JkZXItdG9wLXdpZHRoOiAwcHg7XG4gICAgICAgICAgICAgIGJvcmRlci1ib3R0b20td2lkdGg6IDBweDtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA2MHB4O1xuICAgICAgICAgICAgICBib3JkZXItY29sb3I6ICNkYzM1MjI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAucGxheS5hY3RpdmU6YWZ0ZXIge1xuICAgICAgICAgICAgICBoZWlnaHQ6IDYwcHg7XG4gICAgICAgICAgICAgIGJvcmRlci10b3Atd2lkdGg6IDBweDtcbiAgICAgICAgICAgICAgYm9yZGVyLWJvdHRvbS13aWR0aDogMHB4O1xuICAgICAgICAgICAgICBib3JkZXItbGVmdC13aWR0aDogMjBweDtcbiAgICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgICAgIGJvcmRlci1jb2xvcjogI2RjMzUyMjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC5wbGF5OmZvY3VzIHtcbiAgICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICBgfVxuICAgICAgICA8L3N0eWxlPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBDYXJkO1xuIl19 */\n/*@ sourceURL=components/_card/card-page.js */"
      }));
    }
  }]);

  return Card;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (Card);

/***/ }),

/***/ "./components/_card/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__card_page__ = __webpack_require__("./components/_card/card-page.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__card_page__["a"]; });


/***/ }),

/***/ "./components/_empty-board/emptyBoard-page.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = EmptyBoard;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/components/_empty-board/emptyBoard-page.js";



/* BOARD */
function EmptyBoard(props) {
  return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("li", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    className: "jsx-1718749225" + " " + "empty_board"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("img", {
    src: "http://melanieramosdev.com/img/empty_board.png",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    className: "jsx-1718749225"
  }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    className: "jsx-1718749225"
  }, "Your service is empty!"), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
    styleId: "1718749225",
    css: ".empty_board.jsx-1718749225{text-align:center;color:#c2c2c2;}.empty_board.jsx-1718749225 img.jsx-1718749225{max-height:200px;max-width:228px;margin:20px;}.empty_board.jsx-1718749225 h2.jsx-1718749225{font-size:30px;font-weight:900;margin-bottom:10px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvX2VtcHR5LWJvYXJkL2VtcHR5Qm9hcmQtcGFnZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFNa0IsQUFHNkIsQUFJRCxBQUtGLGVBQ0MsRUFMQSxDQUpGLGFBVUssQ0FUckIsQ0FJYyxZQUNkLEtBS0EiLCJmaWxlIjoiY29tcG9uZW50cy9fZW1wdHktYm9hcmQvZW1wdHlCb2FyZC1wYWdlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9ha2FzaHBhdGhhay9Qcm9qZWN0cy9zbWFydGJlYXIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBCT0FSRCAqL1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gRW1wdHlCb2FyZChwcm9wcykge1xuICByZXR1cm4gKFxuICAgIDxsaSBjbGFzc05hbWU9XCJlbXB0eV9ib2FyZFwiPlxuICAgICAgPGltZyBzcmM9XCJodHRwOi8vbWVsYW5pZXJhbW9zZGV2LmNvbS9pbWcvZW1wdHlfYm9hcmQucG5nXCIgLz5cbiAgICAgIDxoMj5Zb3VyIHNlcnZpY2UgaXMgZW1wdHkhPC9oMj5cbiAgICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgICAgLmVtcHR5X2JvYXJkIHtcbiAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgY29sb3I6ICNjMmMyYzI7XG4gICAgICAgIH1cbiAgICAgICAgLmVtcHR5X2JvYXJkIGltZyB7XG4gICAgICAgICAgbWF4LWhlaWdodDogMjAwcHg7XG4gICAgICAgICAgbWF4LXdpZHRoOiAyMjhweDtcbiAgICAgICAgICBtYXJnaW46IDIwcHg7XG4gICAgICAgIH1cbiAgICAgICAgLmVtcHR5X2JvYXJkIGgyIHtcbiAgICAgICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICB9XG4gICAgICBgfTwvc3R5bGU+XG4gICAgPC9saT5cbiAgKTtcbn1cbiJdfQ== */\n/*@ sourceURL=components/_empty-board/emptyBoard-page.js */"
  }));
}

/***/ }),

/***/ "./components/_empty-board/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__emptyBoard_page__ = __webpack_require__("./components/_empty-board/emptyBoard-page.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__emptyBoard_page__["a"]; });


/***/ }),

/***/ "./components/_modal/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__modal_page__ = __webpack_require__("./components/_modal/modal-page.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__modal_page__["a"]; });


/***/ }),

/***/ "./components/_modal/modal-page.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = ModalPage;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/components/_modal/modal-page.js";


function ModalPage(props) {
  return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    },
    className: "jsx-1525437068" + " " + "modal_background"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    className: "jsx-1525437068" + " " + "modal_window"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    className: "jsx-1525437068" + " " + "top_bar"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    className: "jsx-1525437068"
  }, props.modalTitle), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("button", {
    onClick: props.handleClose,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    className: "jsx-1525437068"
  }, "x")), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("form", {
    onSubmit: function onSubmit(event) {
      return props.handleSubmit(event, props.id);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    className: "jsx-1525437068"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    className: "jsx-1525437068"
  }, "What's the name of service?", __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("input", {
    name: "title",
    type: "text",
    value: props.dirtyFields.title,
    onChange: props.handleInputChange,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    className: "jsx-1525437068"
  })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    },
    className: "jsx-1525437068"
  }, "What the port?", __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("textarea", {
    name: "port",
    value: props.dirtyFields.port,
    onChange: props.handleInputChange,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    className: "jsx-1525437068"
  })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23
    },
    className: "jsx-1525437068"
  }, "Whats the protocol?", __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("textarea", {
    name: "protocol",
    value: props.dirtyFields.protocol,
    onChange: props.handleInputChange,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    className: "jsx-1525437068"
  })), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("label", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    className: "jsx-1525437068"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("input", {
    type: "submit",
    value: "Save",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    className: "jsx-1525437068"
  })))), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
    styleId: "1525437068",
    css: ".modal_background.jsx-1525437068{position:fixed;top:0;bottom:0;left:0;right:0;padding:50px;background-color:rgba(0,0,0,0.3);}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068{border-radius:5px;max-width:500px;min-width:200px;min-height:300px;margin:0 auto;padding:30px;background-color:#fafafa;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 .top_bar.jsx-1525437068{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:10px 10px 20px 10px;background:transparent !important;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 .top_bar.jsx-1525437068 h2.jsx-1525437068{color:#212121;font-size:20px;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 .top_bar.jsx-1525437068 button.jsx-1525437068{height:30px;width:30px;font-size:18px;margin:0;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068{width:100%;height:100%;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068{display:block;margin:20px 10px;color:#212121;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 span.jsx-1525437068{font-size:12px;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 input.jsx-1525437068,.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 textarea.jsx-1525437068,.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 .tags_container.jsx-1525437068{display:block;width:95%;border-radius:5px;border:solid 1px #c2c2c2;margin-top:5px;padding:10px;font-size:14px;font-family:'source-sans-pro','Helvetica Neue',Helvetica,Arial,sans-serif;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 input.jsx-1525437068{height:20px;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 input[type='submit'].jsx-1525437068{height:100%;font-size:12px;width:100%;line-height:2;}.modal_background.jsx-1525437068 .modal_window.jsx-1525437068 form.jsx-1525437068 label.jsx-1525437068 textarea.jsx-1525437068{height:70px;}button.jsx-1525437068,input[type='submit'].jsx-1525437068{background-color:#4066c8;text-shadow:0 2px #4066c8a8;border:none !important;color:#f5f5f5;font-weight:bold;padding-bottom:3px;}button.jsx-1525437068:hover,input[type='submit'].jsx-1525437068:hover{background-color:#4066c8c7;text-shadow:0 2px #4066c8a8;cursor:pointer;}button.jsx-1525437068:active,button.jsx-1525437068:focus,input[type='submit'].jsx-1525437068:active,input[type='submit'].jsx-1525437068:focus{background-color:#4066c8;outline:none;text-shadow:0 2px #4066c8a8;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvX21vZGFsL21vZGFsLXBhZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBb0NTLEFBRzRCLEFBU0csQUFTTCxBQU9DLEFBSUYsQUFNRCxBQUlHLEFBS0MsQUFLRCxBQVVGLEFBR0EsQUFPQSxBQUthLEFBU0UsQUFRRixXQXZEYixDQU5ELEFBOEJiLEFBSWlCLEFBTWpCLEVBNUNpQixBQWNFLEFBVVAsQ0FqREosQUE0Q1IsR0FuQ2tCLEdBUlAsRUE2Qk0sQUFNakIsQ0Fjb0IsQ0F3QlUsQUFpQmYsRUEzQkYsQUFtQmlCLEVBekQ5QixDQXhCUyxDQXNDTyxHQTlCRSxHQVBSLENBNEJDLEFBa0NLLEFBMkJjLElBekNILEdBL0NaLEFBcUNmLEVBVEEsR0FyQm1CLEVBdURuQixDQVN5QixFQVNSLEdBL0VxQixRQXdGdEMsQ0FqRmdCLEFBd0NDLEdBaUNqQixJQW5FZ0MsRUEwRGhCLEtBL0RELENBd0NBLFFBd0JJLENBdkVuQixHQVEyQixDQXdDVixZQXdCSSxHQXZCMkQsU0F4Q2hGLE9BZ0VBLDBEQXZCQSxLQXJDcUIsNkZBQ1MsNEJBQ00sa0NBQ3BDIiwiZmlsZSI6ImNvbXBvbmVudHMvX21vZGFsL21vZGFsLXBhZ2UuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL2FrYXNocGF0aGFrL1Byb2plY3RzL3NtYXJ0YmVhciIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIE1vZGFsUGFnZShwcm9wcykge1xuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPVwibW9kYWxfYmFja2dyb3VuZFwiPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbF93aW5kb3dcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ0b3BfYmFyXCI+XG4gICAgICAgICAgPGgyPntwcm9wcy5tb2RhbFRpdGxlfTwvaDI+XG4gICAgICAgICAgPGJ1dHRvbiBvbkNsaWNrPXtwcm9wcy5oYW5kbGVDbG9zZX0+eDwvYnV0dG9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGZvcm0gb25TdWJtaXQ9e2V2ZW50ID0+IHByb3BzLmhhbmRsZVN1Ym1pdChldmVudCwgcHJvcHMuaWQpfT5cbiAgICAgICAgICA8bGFiZWw+XG4gICAgICAgICAgICBXaGF0J3MgdGhlIG5hbWUgb2Ygc2VydmljZT9cbiAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICBuYW1lPVwidGl0bGVcIlxuICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgIHZhbHVlPXtwcm9wcy5kaXJ0eUZpZWxkcy50aXRsZX1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e3Byb3BzLmhhbmRsZUlucHV0Q2hhbmdlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgIDxsYWJlbD5cbiAgICAgICAgICAgIFdoYXQgdGhlIHBvcnQ/XG4gICAgICAgICAgICA8dGV4dGFyZWEgbmFtZT1cInBvcnRcIiB2YWx1ZT17cHJvcHMuZGlydHlGaWVsZHMucG9ydH0gb25DaGFuZ2U9e3Byb3BzLmhhbmRsZUlucHV0Q2hhbmdlfSAvPlxuICAgICAgICAgIDwvbGFiZWw+XG4gICAgICAgICAgPGxhYmVsPlxuICAgICAgICAgICAgV2hhdHMgdGhlIHByb3RvY29sP1xuICAgICAgICAgICAgPHRleHRhcmVhXG4gICAgICAgICAgICAgIG5hbWU9XCJwcm90b2NvbFwiXG4gICAgICAgICAgICAgIHZhbHVlPXtwcm9wcy5kaXJ0eUZpZWxkcy5wcm90b2NvbH1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e3Byb3BzLmhhbmRsZUlucHV0Q2hhbmdlfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICAgIDxsYWJlbD5cbiAgICAgICAgICAgIDxpbnB1dCB0eXBlPVwic3VibWl0XCIgdmFsdWU9XCJTYXZlXCIgLz5cbiAgICAgICAgICA8L2xhYmVsPlxuICAgICAgICA8L2Zvcm0+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxzdHlsZSBqc3g+XG4gICAgICAgIHtgXG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQge1xuICAgICAgICAgICAgcG9zaXRpb246IGZpeGVkO1xuICAgICAgICAgICAgdG9wOiAwO1xuICAgICAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICAgICAgbGVmdDogMDtcbiAgICAgICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICAgICAgcGFkZGluZzogNTBweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4zKTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQgLm1vZGFsX3dpbmRvdyB7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDUwMHB4O1xuICAgICAgICAgICAgbWluLXdpZHRoOiAyMDBweDtcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDMwMHB4O1xuICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XG4gICAgICAgICAgICBwYWRkaW5nOiAzMHB4O1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZhZmFmYTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQgLm1vZGFsX3dpbmRvdyAudG9wX2JhciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMTBweCAyMHB4IDEwcHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICAgICAgICAgIH1cbiAgICAgICAgICAubW9kYWxfYmFja2dyb3VuZCAubW9kYWxfd2luZG93IC50b3BfYmFyIGgyIHtcbiAgICAgICAgICAgIGNvbG9yOiAjMjEyMTIxO1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICAubW9kYWxfYmFja2dyb3VuZCAubW9kYWxfd2luZG93IC50b3BfYmFyIGJ1dHRvbiB7XG4gICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgICAgICB3aWR0aDogMzBweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICB9XG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQgLm1vZGFsX3dpbmRvdyBmb3JtIHtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgICAgICAgIH1cbiAgICAgICAgICAubW9kYWxfYmFja2dyb3VuZCAubW9kYWxfd2luZG93IGZvcm0gbGFiZWwge1xuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgICAgICBtYXJnaW46IDIwcHggMTBweDtcbiAgICAgICAgICAgIGNvbG9yOiAjMjEyMTIxO1xuICAgICAgICAgIH1cbiAgICAgICAgICAubW9kYWxfYmFja2dyb3VuZCAubW9kYWxfd2luZG93IGZvcm0gbGFiZWwgc3BhbiB7XG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgfVxuICAgICAgICAgIC5tb2RhbF9iYWNrZ3JvdW5kIC5tb2RhbF93aW5kb3cgZm9ybSBsYWJlbCBpbnB1dCxcbiAgICAgICAgICAubW9kYWxfYmFja2dyb3VuZCAubW9kYWxfd2luZG93IGZvcm0gbGFiZWwgdGV4dGFyZWEsXG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQgLm1vZGFsX3dpbmRvdyBmb3JtIGxhYmVsIC50YWdzX2NvbnRhaW5lciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgICAgIHdpZHRoOiA5NSU7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgICAgICBib3JkZXI6IHNvbGlkIDFweCAjYzJjMmMyO1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiAnc291cmNlLXNhbnMtcHJvJywgJ0hlbHZldGljYSBOZXVlJywgSGVsdmV0aWNhLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICAgICAgICB9XG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQgLm1vZGFsX3dpbmRvdyBmb3JtIGxhYmVsIGlucHV0IHtcbiAgICAgICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgICB9XG4gICAgICAgICAgLm1vZGFsX2JhY2tncm91bmQgLm1vZGFsX3dpbmRvdyBmb3JtIGxhYmVsIGlucHV0W3R5cGU9J3N1Ym1pdCddIHtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgIC8qIG1hcmdpbi10b3A6IDMwcHg7ICovXG4gICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyO1xuICAgICAgICAgIH1cbiAgICAgICAgICAubW9kYWxfYmFja2dyb3VuZCAubW9kYWxfd2luZG93IGZvcm0gbGFiZWwgdGV4dGFyZWEge1xuICAgICAgICAgICAgaGVpZ2h0OiA3MHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgICAvKiBCVVRUT05TICovXG4gICAgICAgICAgYnV0dG9uLFxuICAgICAgICAgIGlucHV0W3R5cGU9J3N1Ym1pdCddIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM0MDY2Yzg7XG4gICAgICAgICAgICB0ZXh0LXNoYWRvdzogMCAycHggIzQwNjZjOGE4O1xuICAgICAgICAgICAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBjb2xvcjogI2Y1ZjVmNTtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgcGFkZGluZy1ib3R0b206IDNweDtcbiAgICAgICAgICB9XG4gICAgICAgICAgYnV0dG9uOmhvdmVyLFxuICAgICAgICAgIGlucHV0W3R5cGU9J3N1Ym1pdCddOmhvdmVyIHtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICM0MDY2YzhjNztcbiAgICAgICAgICAgIHRleHQtc2hhZG93OiAwIDJweCAjNDA2NmM4YTg7XG4gICAgICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgICAgICAgfVxuICAgICAgICAgIGJ1dHRvbjphY3RpdmUsXG4gICAgICAgICAgYnV0dG9uOmZvY3VzLFxuICAgICAgICAgIGlucHV0W3R5cGU9J3N1Ym1pdCddOmFjdGl2ZSxcbiAgICAgICAgICBpbnB1dFt0eXBlPSdzdWJtaXQnXTpmb2N1cyB7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDA2NmM4O1xuICAgICAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgICAgIHRleHQtc2hhZG93OiAwIDJweCAjNDA2NmM4YTg7XG4gICAgICAgICAgfVxuICAgICAgICBgfVxuICAgICAgPC9zdHlsZT5cbiAgICA8L2Rpdj5cbiAgKTtcbn1cbiJdfQ== */\n/*@ sourceURL=components/_modal/modal-page.js */"
  }));
}

/***/ }),

/***/ "./components/_protocol/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__protocol_page__ = __webpack_require__("./components/_protocol/protocol-page.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__protocol_page__["a"]; });


/***/ }),

/***/ "./components/_protocol/protocol-page.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Protocol;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/components/_protocol/protocol-page.js";


function Protocol(props) {
  return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    },
    className: "jsx-3129669955" + " " + "tags"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    className: "jsx-3129669955"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    className: "jsx-3129669955"
  }, "Protocol"), " : ", props.protocol), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
    styleId: "3129669955",
    css: ".tags.jsx-3129669955{padding:calc(10px / 2);min-height:20px;background-color:#f5f5f5;cursor:text;}.tags.jsx-3129669955 span.jsx-3129669955{display:inline;padding:1px 5px;margin:0 3px;color:#6e6e6e;cursor:text;font-size:14px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvX3Byb3RvY29sL3Byb3RvY29sLXBhZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBT1MsQUFHb0MsQUFNUixlQUNDLFFBTkEsUUFPSCxRQU5ZLEtBT1gsY0FDRixNQVBBLE1BUUcsTUFQakIsU0FRQSIsImZpbGUiOiJjb21wb25lbnRzL19wcm90b2NvbC9wcm90b2NvbC1wYWdlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9ha2FzaHBhdGhhay9Qcm9qZWN0cy9zbWFydGJlYXIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBQcm90b2NvbChwcm9wcykge1xuICByZXR1cm4gKFxuICAgIDxkaXYgY2xhc3NOYW1lPVwidGFnc1wiPlxuICAgICAgPHNwYW4+XG4gICAgICAgIDxzdHJvbmc+UHJvdG9jb2w8L3N0cm9uZz4gOiB7cHJvcHMucHJvdG9jb2x9XG4gICAgICA8L3NwYW4+XG4gICAgICA8c3R5bGUganN4PlxuICAgICAgICB7YFxuICAgICAgICAgIC50YWdzIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IGNhbGMoMTBweCAvIDIpO1xuICAgICAgICAgICAgbWluLWhlaWdodDogMjBweDtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNWY1ZjU7XG4gICAgICAgICAgICBjdXJzb3I6IHRleHQ7XG4gICAgICAgICAgfVxuICAgICAgICAgIC50YWdzIHNwYW4ge1xuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lO1xuICAgICAgICAgICAgcGFkZGluZzogMXB4IDVweDtcbiAgICAgICAgICAgIG1hcmdpbjogMCAzcHg7XG4gICAgICAgICAgICBjb2xvcjogIzZlNmU2ZTtcbiAgICAgICAgICAgIGN1cnNvcjogdGV4dDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICB9XG4gICAgICAgIGB9XG4gICAgICA8L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufVxuIl19 */\n/*@ sourceURL=components/_protocol/protocol-page.js */"
  }));
}

/***/ }),

/***/ "./components/_top-bar/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__topBar_page__ = __webpack_require__("./components/_top-bar/topBar-page.js");
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__topBar_page__["a"]; });


/***/ }),

/***/ "./components/_top-bar/topBar-page.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = Protocol;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/components/_top-bar/topBar-page.js";


/* CARD TOP BAR */

function Protocol(props) {
  return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    className: "jsx-741297862" + " " + "top_bar"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("h4", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    className: "jsx-741297862" + " " + "port"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("strong", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    className: "jsx-741297862"
  }, "Port: "), props.port), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    className: "jsx-741297862"
  }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("button", {
    onClick: props.onEditButtonClick,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    className: "jsx-741297862" + " " + "edit"
  }, "\u270E")), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
    styleId: "741297862",
    css: ".top_bar.jsx-741297862{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:justify;-webkit-justify-content:space-between;-ms-flex-pack:justify;justify-content:space-between;-webkit-align-items:center;-webkit-box-align:center;-ms-flex-align:center;align-items:center;padding:calc(10px / 2);margin:0;line-height:0;background-color:#f5f5f5;}.top_bar.jsx-741297862 .port.jsx-741297862{color:#a1a1a1;font-size:12px;}.top_bar.jsx-741297862 .edit.jsx-741297862{background-color:transparent;border:none;font-size:1rem;color:#4066c8;margin-left:5px;text-shadow:none;}.top_bar.jsx-741297862 .edit.jsx-741297862:hover{color:#4066c8cf;cursor:pointer;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvX3RvcC1iYXIvdG9wQmFyLXBhZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBYVMsQUFHMEIsQUFTQyxBQUllLEFBUWIsY0FYRCxFQVlBLGFBWGpCLEFBR2MsRUFTZCxVQVJpQixlQUNELGNBQ0UsSUFoQmMsWUFpQmIsaUJBQ25CLHNGQWpCcUIsNkZBQ0ksdUJBQ2QsU0FDSyxjQUNXLHlCQUMzQiIsImZpbGUiOiJjb21wb25lbnRzL190b3AtYmFyL3RvcEJhci1wYWdlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9ha2FzaHBhdGhhay9Qcm9qZWN0cy9zbWFydGJlYXIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBDQVJEIFRPUCBCQVIgKi9cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFByb3RvY29sKHByb3BzKSB7XG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJ0b3BfYmFyXCI+XG4gICAgICA8aDQgY2xhc3NOYW1lPVwicG9ydFwiPjxzdHJvbmc+UG9ydDogPC9zdHJvbmc+e3Byb3BzLnBvcnR9PC9oND5cbiAgICAgIDxkaXY+XG4gICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiZWRpdFwiIG9uQ2xpY2s9e3Byb3BzLm9uRWRpdEJ1dHRvbkNsaWNrfT5cbiAgICAgICAgICAmI3gyNzBFO1xuICAgICAgICA8L2J1dHRvbj5cbiAgICAgIDwvZGl2PlxuICAgICAgPHN0eWxlIGpzeD5cbiAgICAgICAge2BcbiAgICAgICAgICAudG9wX2JhciB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIHBhZGRpbmc6IGNhbGMoMTBweCAvIDIpO1xuICAgICAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDA7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNWY1O1xuICAgICAgICAgIH1cbiAgICAgICAgICAudG9wX2JhciAucG9ydCB7XG4gICAgICAgICAgICBjb2xvcjogI2ExYTFhMTtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgICB9XG4gICAgICAgICAgLnRvcF9iYXIgLmVkaXQge1xuICAgICAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICAgICAgICBjb2xvcjogIzQwNjZjODtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICAgICAgICB0ZXh0LXNoYWRvdzogbm9uZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgLnRvcF9iYXIgLmVkaXQ6aG92ZXIge1xuICAgICAgICAgICAgY29sb3I6ICM0MDY2YzhjZjtcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgICB9XG4gICAgICAgIGB9XG4gICAgICA8L3N0eWxlPlxuICAgIDwvZGl2PlxuICApO1xufVxuIl19 */\n/*@ sourceURL=components/_top-bar/topBar-page.js */"
  }));
}

/***/ }),

/***/ "./helpers/request.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__("axios");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constants__ = __webpack_require__("./helpers/shared/constants.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constants___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__shared_constants__);


/**
 * Create an Axios Client with defaults
 */

var client = __WEBPACK_IMPORTED_MODULE_0_axios___default.a.create({
  baseURL: __WEBPACK_IMPORTED_MODULE_1__shared_constants___default.a.API_ENDPOINT,
  timeout: 60000
});
/**
 * Request Wrapper with default success/error actions
 */

var request = function request(options) {
  var onSuccess = function onSuccess(response) {
    console.debug('Request Successful!', response);
    return response.data;
  };

  var onError = function onError(error) {
    console.error('Request Failed:', error.config);

    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      console.error('Status:', error.response.status);
      console.error('Data:', error.response.data);
      console.error('Headers:', error.response.headers);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      console.error('Error Message:', error.message);
    }

    return Promise.reject(error.response || error.message);
  };

  return client(options).then(onSuccess).catch(onError);
};

/* harmony default export */ __webpack_exports__["a"] = (request);

/***/ }),

/***/ "./helpers/shared/constants.js":
/***/ (function(module, exports) {

module.exports = Object.freeze({
  API_ENDPOINT: "http://localhost:8090/",
  API_VIRTUAL: "sv/v1/virtualizations",
  LOADING: false
});

/***/ }),

/***/ "./helpers/util.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__request__ = __webpack_require__("./helpers/request.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constants__ = __webpack_require__("./helpers/shared/constants.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_constants___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__shared_constants__);



function getVirtualDataObject() {
  return Object(__WEBPACK_IMPORTED_MODULE_0__request__["a" /* default */])({
    url: __WEBPACK_IMPORTED_MODULE_1__shared_constants___default.a.API_VIRTUAL,
    method: 'GET'
  });
}

function EditVirtualization(virtualNumber, data) {
  return Object(__WEBPACK_IMPORTED_MODULE_0__request__["a" /* default */])({
    url: "".concat(__WEBPACK_IMPORTED_MODULE_1__shared_constants___default.a.API_VIRTUAL, "/").concat(virtualNumber),
    method: 'PUT',
    data: data
  });
}

var RequestService = {
  getVirtualDataObject: getVirtualDataObject,
  EditVirtualization: EditVirtualization // , any other api, etc. ...

};
/* harmony default export */ __webpack_exports__["a"] = (RequestService);

/***/ }),

/***/ "./pages/_error.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Error; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react__);
var _jsxFileName = "/Users/akashpathak/Projects/smartbear/pages/_error.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var Error =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Error, _React$Component);

  function Error() {
    _classCallCheck(this, Error);

    return _possibleConstructorReturn(this, (Error.__proto__ || Object.getPrototypeOf(Error)).apply(this, arguments));
  }

  _createClass(Error, [{
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 11
        },
        className: "jsx-678139681" + " " + "error-page"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("header", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 12
        },
        className: "jsx-678139681" + " " + "error-page__header"
      }, __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("img", {
        src: "https://static.tutsplus.com/assets/sad-computer-128aac0432b34e270a8d528fb9e3970b.gif",
        alt: "Sad computer",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 13
        },
        className: "jsx-678139681" + " " + "error-page__header-image"
      }), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("h1", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        },
        className: "jsx-678139681" + " " + "error-page__title nolinks"
      }, "Something went wrong")), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        className: "jsx-678139681" + " " + "error-page__message"
      }, this.props.statusCode ? "An error ".concat(this.props.statusCode, " occurred on server") : 'An error occurred on client'), __WEBPACK_IMPORTED_MODULE_1_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_0_styled_jsx_style___default.a, {
        styleId: "678139681",
        css: ".error-page.jsx-678139681{margin:100px 0 40px;text-align:center;}.error-page__header-image.jsx-678139681{width:112px;}.error-page__title.jsx-678139681{font-size:31px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL19lcnJvci5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUF5QlcsQUFHbUMsQUFLUixBQUlHLFlBSGpCLEdBSUEsS0FUb0Isa0JBQ3BCIiwiZmlsZSI6InBhZ2VzL19lcnJvci5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvYWthc2hwYXRoYWsvUHJvamVjdHMvc21hcnRiZWFyIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRXJyb3IgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBzdGF0aWMgZ2V0SW5pdGlhbFByb3BzKHsgcmVzLCBlcnIgfSkge1xuICAgIGNvbnN0IHN0YXR1c0NvZGUgPSByZXMgPyByZXMuc3RhdHVzQ29kZSA6IGVyciA/IGVyci5zdGF0dXNDb2RlIDogbnVsbDtcbiAgICByZXR1cm4geyBzdGF0dXNDb2RlIH07XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZXJyb3ItcGFnZVwiPlxuICAgICAgICA8aGVhZGVyIGNsYXNzTmFtZT1cImVycm9yLXBhZ2VfX2hlYWRlclwiPlxuICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImVycm9yLXBhZ2VfX2hlYWRlci1pbWFnZVwiXG4gICAgICAgICAgICBzcmM9XCJodHRwczovL3N0YXRpYy50dXRzcGx1cy5jb20vYXNzZXRzL3NhZC1jb21wdXRlci0xMjhhYWMwNDMyYjM0ZTI3MGE4ZDUyOGZiOWUzOTcwYi5naWZcIlxuICAgICAgICAgICAgYWx0PVwiU2FkIGNvbXB1dGVyXCJcbiAgICAgICAgICAvPlxuICAgICAgICAgIDxoMSBjbGFzc05hbWU9XCJlcnJvci1wYWdlX190aXRsZSBub2xpbmtzXCI+U29tZXRoaW5nIHdlbnQgd3Jvbmc8L2gxPlxuICAgICAgICA8L2hlYWRlcj5cbiAgICAgICAgPHAgY2xhc3NOYW1lPVwiZXJyb3ItcGFnZV9fbWVzc2FnZVwiPlxuICAgICAgICAgIHt0aGlzLnByb3BzLnN0YXR1c0NvZGVcbiAgICAgICAgICAgID8gYEFuIGVycm9yICR7dGhpcy5wcm9wcy5zdGF0dXNDb2RlfSBvY2N1cnJlZCBvbiBzZXJ2ZXJgXG4gICAgICAgICAgICA6ICdBbiBlcnJvciBvY2N1cnJlZCBvbiBjbGllbnQnfVxuICAgICAgICA8L3A+XG4gICAgICAgIDxzdHlsZSBqc3g+XG4gICAgICAgICAge2BcbiAgICAgICAgICAgIC5lcnJvci1wYWdlIHtcbiAgICAgICAgICAgICAgbWFyZ2luOiAxMDBweCAwIDQwcHg7XG4gICAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmVycm9yLXBhZ2VfX2hlYWRlci1pbWFnZSB7XG4gICAgICAgICAgICAgIHdpZHRoOiAxMTJweDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLmVycm9yLXBhZ2VfX3RpdGxlIHtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAzMXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIGB9XG4gICAgICAgIDwvc3R5bGU+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG4iXX0= */\n/*@ sourceURL=pages/_error.js */"
      }));
    }
  }], [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var res = _ref.res,
          err = _ref.err;
      var statusCode = res ? res.statusCode : err ? err.statusCode : null;
      return {
        statusCode: statusCode
      };
    }
  }]);

  return Error;
}(__WEBPACK_IMPORTED_MODULE_1_react___default.a.Component);



/***/ }),

/***/ "./pages/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__("@babel/runtime/regenerator");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__ = __webpack_require__("styled-jsx/style");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_axios__ = __webpack_require__("axios");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_board__ = __webpack_require__("./components/_board/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__error__ = __webpack_require__("./pages/_error.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants__ = __webpack_require__("./helpers/shared/constants.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__helpers_util__ = __webpack_require__("./helpers/util.js");

var _jsxFileName = "/Users/akashpathak/Projects/smartbear/pages/index.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // Component



 // Helper for Data fetching


var Loading = __WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants___default.a.LOADING;

var Home =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Home, _React$Component);

  _createClass(Home, null, [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee() {
        var getVirtualDataObject;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return __WEBPACK_IMPORTED_MODULE_7__helpers_util__["a" /* default */].getVirtualDataObject().then(function (response) {
                  return response.virtualizationList;
                }).catch(function (error) {
                  return error;
                });

              case 2:
                getVirtualDataObject = _context.sent;
                return _context.abrupt("return", _objectSpread({}, this.props, {
                  getVirtualDataObject: getVirtualDataObject
                }));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps() {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  function Home(props) {
    var _this;

    _classCallCheck(this, Home);

    _this = _possibleConstructorReturn(this, (Home.__proto__ || Object.getPrototypeOf(Home)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "getVirtualDataFunc", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function () {
        var _value = _asyncToGenerator(
        /*#__PURE__*/
        __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2() {
          var getVirtualDataObject;
          return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.prev = 0;
                  _context2.next = 3;
                  return __WEBPACK_IMPORTED_MODULE_7__helpers_util__["a" /* default */].getVirtualDataObject();

                case 3:
                  getVirtualDataObject = _context2.sent;
                  return _context2.abrupt("return", getVirtualDataObject);

                case 7:
                  _context2.prev = 7;
                  _context2.t0 = _context2["catch"](0);
                  return _context2.abrupt("return", _context2.t0);

                case 10:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this, [[0, 7]]);
        }));

        return function value() {
          return _value.apply(this, arguments);
        };
      }()
    });
    Object.defineProperty(_assertThisInitialized(_this), "isRunningToggle", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function () {
        var _value2 = _asyncToGenerator(
        /*#__PURE__*/
        __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee3(id, runningStatus) {
          var toggleRunning;
          return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.prev = 0;
                  _context3.next = 3;
                  return __WEBPACK_IMPORTED_MODULE_7__helpers_util__["a" /* default */].EditVirtualization(id, runningStatus);

                case 3:
                  toggleRunning = _context3.sent;
                  toggleRunning === "OK" && _this.getVirtualDataFunc().then(function (res) {
                    return _this.setState({
                      getVirtualDataObject: res.virtualizationList
                    });
                  });
                  return _context3.abrupt("return", toggleRunning);

                case 8:
                  _context3.prev = 8;
                  _context3.t0 = _context3["catch"](0);
                  return _context3.abrupt("return", _context3.t0);

                case 11:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3, this, [[0, 8]]);
        }));

        return function value(_x, _x2) {
          return _value2.apply(this, arguments);
        };
      }()
    });
    _this.state = {
      getVirtualDataObject: props.getVirtualDataObject
    };
    return _this;
  } // Make a seperate call


  _createClass(Home, [{
    key: "render",
    value: function render() {
      var getVirtualDataObject = this.state.getVirtualDataObject;

      if (_typeof(getVirtualDataObject) !== "object") {
        return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__error__["default"], {
          statusCode: getVirtualDataObject,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 66
          }
        });
      }

      console.log(this.state);
      return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        className: "jsx-3624804388" + " " + "list"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__components_board__["a" /* default */], {
        getVirtualDataObject: getVirtualDataObject,
        isRunningToggle: this.isRunningToggle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        }
      }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default.a, {
        styleId: "3624804388",
        css: "@import url(\"https://fonts.googleapis.com/css?family=Open+Sans:400,700\");.list.jsx-3624804388{font-family:\"Open Sans\",sans-serif;padding:50px;text-align:center;background-color:#ddd;}.photo.jsx-3624804388{display:inline-block;}.photoLink.jsx-3624804388{color:#333;verticalalign:middle;cursor:pointer;background:#eee;display:inline-block;width:250px;height:250px;line-height:250px;margin:10px;border:2px solid transparent;}.photoLink.jsx-3624804388:hover{bordercolor:blue;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTBFb0IsQUFFbUYsQUFFbEMsQUFPZixBQUlWLEFBYU0sV0FaSSxNQWF2QixJQWpCQSxXQUtpQixHQVpGLFlBYUcsQ0FaRSxlQWFHLEdBWkMsa0JBYVYsSUFaZCxRQWFlLGFBQ0ssa0JBQ04sWUFDaUIsNkJBQy9CIiwiZmlsZSI6InBhZ2VzL2luZGV4LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9ha2FzaHBhdGhhay9Qcm9qZWN0cy9zbWFydGJlYXIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgYXhpb3MgZnJvbSBcImF4aW9zXCI7XG4vLyBDb21wb25lbnRcbmltcG9ydCBCb2FyZCBmcm9tIFwiLi4vY29tcG9uZW50cy9fYm9hcmRcIjtcbmltcG9ydCBFcnJvcnBhZ2UgZnJvbSBcIi4vX2Vycm9yXCI7XG5pbXBvcnQgY29uc3RhbnRzIGZyb20gXCIuLi9oZWxwZXJzL3NoYXJlZC9jb25zdGFudHNcIjtcblxuLy8gSGVscGVyIGZvciBEYXRhIGZldGNoaW5nXG5pbXBvcnQgUmVxdWVzdFNlcnZpY2UgZnJvbSBcIi4uL2hlbHBlcnMvdXRpbFwiO1xubGV0IExvYWRpbmcgPSBjb25zdGFudHMuTE9BRElORztcblxuY2xhc3MgSG9tZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBhc3luYyBnZXRJbml0aWFsUHJvcHMoKSB7XG4gICAgbGV0IGdldFZpcnR1YWxEYXRhT2JqZWN0ID0gYXdhaXQgUmVxdWVzdFNlcnZpY2UuZ2V0VmlydHVhbERhdGFPYmplY3QoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UudmlydHVhbGl6YXRpb25MaXN0O1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBlcnJvcjtcbiAgICAgIH0pO1xuICAgIHJldHVybiB7XG4gICAgICAuLi50aGlzLnByb3BzLFxuICAgICAgZ2V0VmlydHVhbERhdGFPYmplY3RcbiAgICB9O1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGdldFZpcnR1YWxEYXRhT2JqZWN0OiBwcm9wcy5nZXRWaXJ0dWFsRGF0YU9iamVjdFxuICAgIH07XG4gIH1cblxuICAvLyBNYWtlIGEgc2VwZXJhdGUgY2FsbFxuICBnZXRWaXJ0dWFsRGF0YUZ1bmMgPSBhc3luYyAoKSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgIGxldCBnZXRWaXJ0dWFsRGF0YU9iamVjdCA9IGF3YWl0IFJlcXVlc3RTZXJ2aWNlLmdldFZpcnR1YWxEYXRhT2JqZWN0KCk7XG4gICAgICByZXR1cm4gZ2V0VmlydHVhbERhdGFPYmplY3Q7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICByZXR1cm4gZXJyO1xuICAgIH1cbiAgfTtcblxuICAvLyBTZXBlcmF0ZWQgcHV0IGNhbGxcbiAgaXNSdW5uaW5nVG9nZ2xlID0gYXN5bmMgKGlkLCBydW5uaW5nU3RhdHVzKSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgIGxldCB0b2dnbGVSdW5uaW5nID0gYXdhaXQgUmVxdWVzdFNlcnZpY2UuRWRpdFZpcnR1YWxpemF0aW9uKFxuICAgICAgICBpZCxcbiAgICAgICAgcnVubmluZ1N0YXR1c1xuICAgICAgKTtcbiAgICAgIHRvZ2dsZVJ1bm5pbmcgPT09IFwiT0tcIiAmJlxuICAgICAgICB0aGlzLmdldFZpcnR1YWxEYXRhRnVuYygpLnRoZW4ocmVzID0+XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBnZXRWaXJ0dWFsRGF0YU9iamVjdDogcmVzLnZpcnR1YWxpemF0aW9uTGlzdFxuICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgICByZXR1cm4gdG9nZ2xlUnVubmluZztcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIHJldHVybiBlcnI7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGdldFZpcnR1YWxEYXRhT2JqZWN0IH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmICh0eXBlb2YgZ2V0VmlydHVhbERhdGFPYmplY3QgIT09IFwib2JqZWN0XCIpIHtcbiAgICAgIHJldHVybiA8RXJyb3JwYWdlIHN0YXR1c0NvZGU9e2dldFZpcnR1YWxEYXRhT2JqZWN0fSAvPjtcbiAgICB9XG4gICAgY29uc29sZS5sb2codGhpcy5zdGF0ZSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGlzdFwiPlxuICAgICAgICA8Qm9hcmRcbiAgICAgICAgICBnZXRWaXJ0dWFsRGF0YU9iamVjdD17Z2V0VmlydHVhbERhdGFPYmplY3R9XG4gICAgICAgICAgaXNSdW5uaW5nVG9nZ2xlPXt0aGlzLmlzUnVubmluZ1RvZ2dsZX1cbiAgICAgICAgLz5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgIEBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PU9wZW4rU2Fuczo0MDAsNzAwXCIpO1xuICAgICAgICAgIC5saXN0IHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICAgICAgICAgICAgcGFkZGluZzogNTBweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLnBob3RvIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAucGhvdG9MaW5rIHtcbiAgICAgICAgICAgIGNvbG9yOiAjMzMzO1xuICAgICAgICAgICAgdmVydGljYWxhbGlnbjogbWlkZGxlO1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgIHdpZHRoOiAyNTBweDtcbiAgICAgICAgICAgIGhlaWdodDogMjUwcHg7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjUwcHg7XG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAucGhvdG9MaW5rOmhvdmVyIHtcbiAgICAgICAgICAgIGJvcmRlcmNvbG9yOiBibHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgYH08L3N0eWxlPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBIb21lO1xuIl19 */\n/*@ sourceURL=pages/index.js */"
      }));
    }
  }]);

  return Home;
}(__WEBPACK_IMPORTED_MODULE_2_react___default.a.Component);

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./pages/index.js");


/***/ }),

/***/ "@babel/runtime/regenerator":
/***/ (function(module, exports) {

module.exports = require("@babel/runtime/regenerator");

/***/ }),

/***/ "axios":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "react":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map