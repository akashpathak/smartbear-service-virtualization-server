webpackHotUpdate(4,{

/***/ "./pages/index.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__ = __webpack_require__("./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__ = __webpack_require__("./node_modules/styled-jsx/style.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_axios__ = __webpack_require__("./node_modules/axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_board__ = __webpack_require__("./components/_board/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__error__ = __webpack_require__("./pages/_error.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants__ = __webpack_require__("./helpers/shared/constants.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__helpers_util__ = __webpack_require__("./helpers/util.js");

var _jsxFileName = "/Users/akashpathak/Projects/smartbear/pages/index.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } } function _next(value) { step("next", value); } function _throw(err) { step("throw", err); } _next(); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }


 // Component



 // Helper for Data fetching


var Loading = __WEBPACK_IMPORTED_MODULE_6__helpers_shared_constants___default.a.LOADING;

var Home =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Home, _React$Component);

  _createClass(Home, null, [{
    key: "getInitialProps",
    value: function () {
      var _getInitialProps = _asyncToGenerator(
      /*#__PURE__*/
      __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee() {
        var getVirtualDataObject;
        return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return __WEBPACK_IMPORTED_MODULE_7__helpers_util__["a" /* default */].getVirtualDataObject().then(function (response) {
                  return response.virtualizationList;
                }).catch(function (error) {
                  return error;
                });

              case 2:
                getVirtualDataObject = _context.sent;
                return _context.abrupt("return", _objectSpread({}, this.props, {
                  getVirtualDataObject: getVirtualDataObject
                }));

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function getInitialProps() {
        return _getInitialProps.apply(this, arguments);
      };
    }()
  }]);

  function Home(props) {
    var _this;

    _classCallCheck(this, Home);

    _this = _possibleConstructorReturn(this, (Home.__proto__ || Object.getPrototypeOf(Home)).call(this, props));
    Object.defineProperty(_assertThisInitialized(_this), "getVirtualDataFunc", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function () {
        var _value = _asyncToGenerator(
        /*#__PURE__*/
        __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee2() {
          var getVirtualDataObject;
          return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  _context2.prev = 0;
                  _context2.next = 3;
                  return __WEBPACK_IMPORTED_MODULE_7__helpers_util__["a" /* default */].getVirtualDataObject();

                case 3:
                  getVirtualDataObject = _context2.sent;
                  return _context2.abrupt("return", getVirtualDataObject);

                case 7:
                  _context2.prev = 7;
                  _context2.t0 = _context2["catch"](0);
                  return _context2.abrupt("return", _context2.t0);

                case 10:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, this, [[0, 7]]);
        }));

        return function value() {
          return _value.apply(this, arguments);
        };
      }()
    });
    Object.defineProperty(_assertThisInitialized(_this), "isRunningToggle", {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function () {
        var _value2 = _asyncToGenerator(
        /*#__PURE__*/
        __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.mark(function _callee3(id, runningStatus) {
          var toggleRunning;
          return __WEBPACK_IMPORTED_MODULE_0__babel_runtime_regenerator___default.a.wrap(function _callee3$(_context3) {
            while (1) {
              switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.prev = 0;
                  _context3.next = 3;
                  return __WEBPACK_IMPORTED_MODULE_7__helpers_util__["a" /* default */].EditVirtualization(id, runningStatus);

                case 3:
                  toggleRunning = _context3.sent;
                  toggleRunning === "OK" && _this.getVirtualDataFunc().then(function (res) {
                    return _this.setState({
                      getVirtualDataObject: res.virtualizationList
                    });
                  });
                  return _context3.abrupt("return", toggleRunning);

                case 8:
                  _context3.prev = 8;
                  _context3.t0 = _context3["catch"](0);
                  return _context3.abrupt("return", _context3.t0);

                case 11:
                case "end":
                  return _context3.stop();
              }
            }
          }, _callee3, this, [[0, 8]]);
        }));

        return function value(_x, _x2) {
          return _value2.apply(this, arguments);
        };
      }()
    });
    _this.state = {
      getVirtualDataObject: props.getVirtualDataObject
    };
    return _this;
  } // Make a seperate call


  _createClass(Home, [{
    key: "render",
    value: function render() {
      var getVirtualDataObject = this.state.getVirtualDataObject;

      if (_typeof(getVirtualDataObject) !== "object") {
        return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__error__["default"], {
          statusCode: getVirtualDataObject,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 66
          }
        });
      }

      console.log(this.state);
      return __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        className: "jsx-3624804388" + " " + "list"
      }, __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__components_board__["a" /* default */], {
        getVirtualDataObject: getVirtualDataObject,
        isRunningToggle: this.isRunningToggle,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        }
      }), __WEBPACK_IMPORTED_MODULE_2_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_styled_jsx_style___default.a, {
        styleId: "3624804388",
        css: "@import url(\"https://fonts.googleapis.com/css?family=Open+Sans:400,700\");.list.jsx-3624804388{font-family:\"Open Sans\",sans-serif;padding:50px;text-align:center;background-color:#ddd;}.photo.jsx-3624804388{display:inline-block;}.photoLink.jsx-3624804388{color:#333;verticalalign:middle;cursor:pointer;background:#eee;display:inline-block;width:250px;height:250px;line-height:250px;margin:10px;border:2px solid transparent;}.photoLink.jsx-3624804388:hover{bordercolor:blue;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTBFb0IsQUFFbUYsQUFFbEMsQUFPZixBQUlWLEFBYU0sV0FaSSxNQWF2QixJQWpCQSxXQUtpQixHQVpGLFlBYUcsQ0FaRSxlQWFHLEdBWkMsa0JBYVYsSUFaZCxRQWFlLGFBQ0ssa0JBQ04sWUFDaUIsNkJBQy9CIiwiZmlsZSI6InBhZ2VzL2luZGV4LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9ha2FzaHBhdGhhay9Qcm9qZWN0cy9zbWFydGJlYXIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgYXhpb3MgZnJvbSBcImF4aW9zXCI7XG4vLyBDb21wb25lbnRcbmltcG9ydCBCb2FyZCBmcm9tIFwiLi4vY29tcG9uZW50cy9fYm9hcmRcIjtcbmltcG9ydCBFcnJvcnBhZ2UgZnJvbSBcIi4vX2Vycm9yXCI7XG5pbXBvcnQgY29uc3RhbnRzIGZyb20gXCIuLi9oZWxwZXJzL3NoYXJlZC9jb25zdGFudHNcIjtcblxuLy8gSGVscGVyIGZvciBEYXRhIGZldGNoaW5nXG5pbXBvcnQgUmVxdWVzdFNlcnZpY2UgZnJvbSBcIi4uL2hlbHBlcnMvdXRpbFwiO1xubGV0IExvYWRpbmcgPSBjb25zdGFudHMuTE9BRElORztcblxuY2xhc3MgSG9tZSBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBhc3luYyBnZXRJbml0aWFsUHJvcHMoKSB7XG4gICAgbGV0IGdldFZpcnR1YWxEYXRhT2JqZWN0ID0gYXdhaXQgUmVxdWVzdFNlcnZpY2UuZ2V0VmlydHVhbERhdGFPYmplY3QoKVxuICAgICAgLnRoZW4ocmVzcG9uc2UgPT4ge1xuICAgICAgICByZXR1cm4gcmVzcG9uc2UudmlydHVhbGl6YXRpb25MaXN0O1xuICAgICAgfSlcbiAgICAgIC5jYXRjaChlcnJvciA9PiB7XG4gICAgICAgIHJldHVybiBlcnJvcjtcbiAgICAgIH0pO1xuICAgIHJldHVybiB7XG4gICAgICAuLi50aGlzLnByb3BzLFxuICAgICAgZ2V0VmlydHVhbERhdGFPYmplY3RcbiAgICB9O1xuICB9XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGdldFZpcnR1YWxEYXRhT2JqZWN0OiBwcm9wcy5nZXRWaXJ0dWFsRGF0YU9iamVjdFxuICAgIH07XG4gIH1cblxuICAvLyBNYWtlIGEgc2VwZXJhdGUgY2FsbFxuICBnZXRWaXJ0dWFsRGF0YUZ1bmMgPSBhc3luYyAoKSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgIGxldCBnZXRWaXJ0dWFsRGF0YU9iamVjdCA9IGF3YWl0IFJlcXVlc3RTZXJ2aWNlLmdldFZpcnR1YWxEYXRhT2JqZWN0KCk7XG4gICAgICByZXR1cm4gZ2V0VmlydHVhbERhdGFPYmplY3Q7XG4gICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICByZXR1cm4gZXJyO1xuICAgIH1cbiAgfTtcblxuICAvLyBTZXBlcmF0ZWQgcHV0IGNhbGxcbiAgaXNSdW5uaW5nVG9nZ2xlID0gYXN5bmMgKGlkLCBydW5uaW5nU3RhdHVzKSA9PiB7XG4gICAgdHJ5IHtcbiAgICAgIGxldCB0b2dnbGVSdW5uaW5nID0gYXdhaXQgUmVxdWVzdFNlcnZpY2UuRWRpdFZpcnR1YWxpemF0aW9uKFxuICAgICAgICBpZCxcbiAgICAgICAgcnVubmluZ1N0YXR1c1xuICAgICAgKTtcbiAgICAgIHRvZ2dsZVJ1bm5pbmcgPT09IFwiT0tcIiAmJlxuICAgICAgICB0aGlzLmdldFZpcnR1YWxEYXRhRnVuYygpLnRoZW4ocmVzID0+XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBnZXRWaXJ0dWFsRGF0YU9iamVjdDogcmVzLnZpcnR1YWxpemF0aW9uTGlzdFxuICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgICByZXR1cm4gdG9nZ2xlUnVubmluZztcbiAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgIHJldHVybiBlcnI7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGdldFZpcnR1YWxEYXRhT2JqZWN0IH0gPSB0aGlzLnN0YXRlO1xuICAgIGlmICh0eXBlb2YgZ2V0VmlydHVhbERhdGFPYmplY3QgIT09IFwib2JqZWN0XCIpIHtcbiAgICAgIHJldHVybiA8RXJyb3JwYWdlIHN0YXR1c0NvZGU9e2dldFZpcnR1YWxEYXRhT2JqZWN0fSAvPjtcbiAgICB9XG4gICAgY29uc29sZS5sb2codGhpcy5zdGF0ZSk7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwibGlzdFwiPlxuICAgICAgICA8Qm9hcmRcbiAgICAgICAgICBnZXRWaXJ0dWFsRGF0YU9iamVjdD17Z2V0VmlydHVhbERhdGFPYmplY3R9XG4gICAgICAgICAgaXNSdW5uaW5nVG9nZ2xlPXt0aGlzLmlzUnVubmluZ1RvZ2dsZX1cbiAgICAgICAgLz5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgIEBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PU9wZW4rU2Fuczo0MDAsNzAwXCIpO1xuICAgICAgICAgIC5saXN0IHtcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcIk9wZW4gU2Fuc1wiLCBzYW5zLXNlcmlmO1xuICAgICAgICAgICAgcGFkZGluZzogNTBweDtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgLnBob3RvIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAucGhvdG9MaW5rIHtcbiAgICAgICAgICAgIGNvbG9yOiAjMzMzO1xuICAgICAgICAgICAgdmVydGljYWxhbGlnbjogbWlkZGxlO1xuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICAgIHdpZHRoOiAyNTBweDtcbiAgICAgICAgICAgIGhlaWdodDogMjUwcHg7XG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMjUwcHg7XG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XG4gICAgICAgICAgICBib3JkZXI6IDJweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAucGhvdG9MaW5rOmhvdmVyIHtcbiAgICAgICAgICAgIGJvcmRlcmNvbG9yOiBibHVlO1xuICAgICAgICAgIH1cbiAgICAgICAgYH08L3N0eWxlPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBIb21lO1xuIl19 */\n/*@ sourceURL=pages/index.js */"
      }));
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return Home;
}(__WEBPACK_IMPORTED_MODULE_2_react___default.a.Component);

var _default = Home;
/* harmony default export */ __webpack_exports__["default"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Loading, "Loading", "/Users/akashpathak/Projects/smartbear/pages/index.js");
  reactHotLoader.register(Home, "Home", "/Users/akashpathak/Projects/smartbear/pages/index.js");
  reactHotLoader.register(_default, "default", "/Users/akashpathak/Projects/smartbear/pages/index.js");
  leaveModule(module);
})();

;
    (function (Component, route) {
      if(!Component) return
      if (false) return
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/")
  
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=4.db0ea4cb5451219b02d2.hot-update.js.map