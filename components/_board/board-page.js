//Components
import Cards from "../_card";
import EmptyBoard from "../_empty-board";

export default function Board(props) {
  return (
    <div>
      <ul id="board">
        {props.getVirtualDataObject.length <= 0 ? (
          <EmptyBoard />
        ) : (
          props.getVirtualDataObject.map(virtualData => (
            <li key={virtualData.virtualizationID}>
              <Cards
                id={virtualData.virtualizationID}
                port={virtualData.port}
                title={virtualData.name}
                apiType={virtualData.apiType}
                running={virtualData.running}
                protocol={virtualData.protocol}
                isRunningToggle={props.isRunningToggle}
              />
            </li>
          ))
        )}
      </ul>
      <style jsx>{`
        #board {
          list-style: none;
          display: flex;
          flex-flow: row wrap;
          justify-content: center;
          align-items: start;
          height: auto;
          padding: 30px 0;
          margin: 0;
        }
      `}</style>
    </div>
  );
}
