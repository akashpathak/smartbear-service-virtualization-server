export default function Protocol(props) {
  return (
    <div className="tags">
      <span>
        <strong>Protocol</strong> : {props.protocol}
      </span>
      <style jsx>
        {`
          .tags {
            padding: calc(10px / 2);
            min-height: 20px;
            background-color: #f5f5f5;
            cursor: text;
          }
          .tags span {
            display: inline;
            padding: 1px 5px;
            margin: 0 3px;
            color: #6e6e6e;
            cursor: text;
            font-size: 14px;
          }
        `}
      </style>
    </div>
  );
}
