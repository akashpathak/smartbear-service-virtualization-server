/* BOARD */
export default function EmptyBoard(props) {
  return (
    <li className="empty_board">
      <img src="http://melanieramosdev.com/img/empty_board.png" />
      <h2>Your service is empty!</h2>
      <style jsx>{`
        .empty_board {
          text-align: center;
          color: #c2c2c2;
        }
        .empty_board img {
          max-height: 200px;
          max-width: 228px;
          margin: 20px;
        }
        .empty_board h2 {
          font-size: 30px;
          font-weight: 900;
          margin-bottom: 10px;
        }
      `}</style>
    </li>
  );
}
