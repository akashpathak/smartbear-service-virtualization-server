import React, { Component } from "react";
//Component
import ModalPage from "../_modal";
import TopBar from "../_top-bar";
import Protocol from "../_protocol";

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      running: props.running,
      editCard:'',
      dirtyFields: {
        port: props.port,
        protocol: props.protocol,
        title: props.title
      }
    };
  }

  handleModal = (isOpen) => {
    this.setState({ editCard: isOpen });
  };

  // Open modal
  handleOpen = () => {
    this.handleModal(true);
  };

  // Edit modal fields
  handleInputChange = e => {
    const { name, value } = e.target;
    this.setState({
      dirtyFields: {
        ...this.state.dirtyFields,
        [name]: value
      }
    });
  };

  // Handle form submit
  handleSubmit = (event, id) => {
    event.preventDefault();
    // We need to make api call from here
    let postingData = {};
    if (this.state.dirtyFields.title !== this.props.title) {
      postingData["name"] = this.state.dirtyFields.title;
    } else if (this.state.dirtyFields.port !== this.props.port) {
      postingData["port"] = this.state.dirtyFields.port;
    } else if (this.state.dirtyFields.protocol !== this.props.protocol) {
      postingData["protocol"] = this.state.dirtyFields.protocol;
    }
    // Checking if putData is empty object then dont make a call
    if (
      Object.keys(postingData).length === 0 &&
      postingData.constructor === Object
    ) {
      this.handleModal(false);
    } else {
      this.props
        .isRunningToggle(id, postingData)
        .then(res => res === "OK" && this.handleModal(false));
    }
  };

  // Handle Start/Stop service
  handleStartStop = (event, id, kind) => {
    event.preventDefault();
    console.log(kind);
    let StatusObject = {
      running: !this.state.running
    };
    this.props.isRunningToggle(id, StatusObject).then(
      res =>
        res === "OK" &&
        this.setState({
          running: !kind
        })
    );
  };

  // Handle Modal close
  handleClose = () => {
    this.handleModal(false);
  };

  render() {
    return (
      <div className="card">
        {this.state.editCard && (
          <ModalPage
            onModalToggle={this.handleModal}
            modalTitle="Edit this service"
            id={this.props.id}
            dirtyFields={this.state.dirtyFields}
            handleClose={this.handleClose}
            handleSubmit={this.handleSubmit}
            handleInputChange={this.handleInputChange}
          />
        )}
        <TopBar
          id={this.props.id}
          port={this.props.port}
          title={this.props.title}
          onEditButtonClick={this.handleOpen}
        />
        <h2 className="title">{this.props.title}</h2>
        <div className="description">
          <p>{this.props.apiType}</p>
          <button
            className={this.state.running ? "play" : "play active"}
            onClick={event =>
              this.handleStartStop(event, this.props.id, this.state.running)
            }
          />
        </div>
        <Protocol protocol={this.props.protocol} />
        <style jsx>
          {`
            .card {
              display: flex;
              text-align: left;
              flex-direction: column;
              justify-content: space-between;
              height: auto;
              width: 33rem;
              max-width: 100vw;
              flex-basis: auto; /* default value */
              flex-grow: 1;
              margin: 20px;
              background-color: #fafafa;
              box-shadow: 0 0.5rem 3rem rgba(0, 0, 0, 0.1);
               {
                /* border-radius: 0.7rem; */
              }
            }
            .card .title {
              color: #212121;
              font-size: 18px;
              padding: calc(10px / 2);
              text-transform: uppercase;
              margin: 0;
              color: #fff;
              background-color: #4066c8;
            }
            .card .description {
              text-align: top;
              min-height: 120px;
            }
            .card .description p {
              color: #212121;
              font-size: 16px;
              padding: calc(10px / 2);
              margin: 0;
              font-weight: bold;
              border-left: 5px solid #4066c8;
            }
            .play {
              cursor: pointer;
              display: inline-block;
              padding: 0.75rem;
              width: 100%;
              background: transparent;
              border: 0;
              text-align: left;
              width: 20%;
            }
            .play:before,
            .play:after {
              content: "";
              display: inline-block;
              vertical-align: middle;
              transition: all 0.75s;
            }
            .play:before {
              height: 40px;
              border: 20px solid #9c0;
              border-right: none;
              border-top: 13px solid transparent;
              border-bottom: 13px solid transparent;
            }
            .play:after {
              margin: 5px 0;
              height: 0px;
              border: 30px solid #9c0;
              border-right: none;
              border-top: 20px solid transparent;
              border-bottom: 20px solid transparent;
            }
            .play.active:before {
              border-top-width: 0px;
              border-bottom-width: 0px;
              height: 60px;
              border-color: #dc3522;
            }
            .play.active:after {
              height: 60px;
              border-top-width: 0px;
              border-bottom-width: 0px;
              border-left-width: 20px;
              margin-left: 10px;
              border-color: #dc3522;
            }
            .play:focus {
              outline: none;
            }
          `}
        </style>
      </div>
    );
  }
}

export default Card;
