/* CARD TOP BAR */
import React from 'react';

export default function Protocol(props) {
  return (
    <div className="top_bar">
      <h4 className="port"><strong>Port: </strong>{props.port}</h4>
      <div>
        <button className="edit" onClick={props.onEditButtonClick}>
          &#x270E;
        </button>
      </div>
      <style jsx>
        {`
          .top_bar {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: calc(10px / 2);
            margin: 0;
            line-height: 0;
            background-color: #f5f5f5;
          }
          .top_bar .port {
            color: #a1a1a1;
            font-size: 12px;
          }
          .top_bar .edit {
            background-color: transparent;
            border: none;
            font-size: 1rem;
            color: #4066c8;
            margin-left: 5px;
            text-shadow: none;
          }
          .top_bar .edit:hover {
            color: #4066c8cf;
            cursor: pointer;
          }
        `}
      </style>
    </div>
  );
}
