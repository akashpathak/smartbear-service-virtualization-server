export default function ModalPage(props) {
  return (
    <div className="modal_background">
      <div className="modal_window">
        <div className="top_bar">
          <h2>{props.modalTitle}</h2>
          <button onClick={props.handleClose}>x</button>
        </div>
        <form onSubmit={event => props.handleSubmit(event, props.id)}>
          <label>
            What's the name of service?
            <input
              name="title"
              type="text"
              value={props.dirtyFields.title}
              onChange={props.handleInputChange}
            />
          </label>
          <label>
            What the port?
            <textarea name="port" value={props.dirtyFields.port} onChange={props.handleInputChange} />
          </label>
          <label>
            Whats the protocol?
            <textarea
              name="protocol"
              value={props.dirtyFields.protocol}
              onChange={props.handleInputChange}
            />
          </label>
          <label>
            <input type="submit" value="Save" />
          </label>
        </form>
      </div>
      <style jsx>
        {`
          .modal_background {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            padding: 50px;
            background-color: rgba(0, 0, 0, 0.3);
          }
          .modal_background .modal_window {
            border-radius: 5px;
            max-width: 500px;
            min-width: 200px;
            min-height: 300px;
            margin: 0 auto;
            padding: 30px;
            background-color: #fafafa;
          }
          .modal_background .modal_window .top_bar {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 10px 10px 20px 10px;
            background: transparent !important;
          }
          .modal_background .modal_window .top_bar h2 {
            color: #212121;
            font-size: 20px;
          }
          .modal_background .modal_window .top_bar button {
            height: 30px;
            width: 30px;
            font-size: 18px;
            margin: 0;
          }
          .modal_background .modal_window form {
            width: 100%;
            height: 100%;
          }
          .modal_background .modal_window form label {
            display: block;
            margin: 20px 10px;
            color: #212121;
          }
          .modal_background .modal_window form label span {
            font-size: 12px;
          }
          .modal_background .modal_window form label input,
          .modal_background .modal_window form label textarea,
          .modal_background .modal_window form label .tags_container {
            display: block;
            width: 95%;
            border-radius: 5px;
            border: solid 1px #c2c2c2;
            margin-top: 5px;
            padding: 10px;
            font-size: 14px;
            font-family: 'source-sans-pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
          }
          .modal_background .modal_window form label input {
            height: 20px;
          }
          .modal_background .modal_window form label input[type='submit'] {
            height: 100%;
            /* margin-top: 30px; */
            font-size: 12px;
            width: 100%;
            line-height: 2;
          }
          .modal_background .modal_window form label textarea {
            height: 70px;
          }
          /* BUTTONS */
          button,
          input[type='submit'] {
            background-color: #4066c8;
            text-shadow: 0 2px #4066c8a8;
            border: none !important;
            color: #f5f5f5;
            font-weight: bold;
            padding-bottom: 3px;
          }
          button:hover,
          input[type='submit']:hover {
            background-color: #4066c8c7;
            text-shadow: 0 2px #4066c8a8;
            cursor: pointer;
          }
          button:active,
          button:focus,
          input[type='submit']:active,
          input[type='submit']:focus {
            background-color: #4066c8;
            outline: none;
            text-shadow: 0 2px #4066c8a8;
          }
        `}
      </style>
    </div>
  );
}
