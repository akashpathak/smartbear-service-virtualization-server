import request from './request';
import constants from './shared/constants';

function getVirtualDataObject() {
  return request({
    url: constants.API_VIRTUAL,
    method: 'GET',
  });
}

function EditVirtualization(virtualNumber, data ) {
  return request({
    url: `${constants.API_VIRTUAL}/${virtualNumber}`,
    method: 'PUT',
   data
  });
}

const RequestService = {
  getVirtualDataObject,
  EditVirtualization, // , any other api, etc. ...
};

export default RequestService;
