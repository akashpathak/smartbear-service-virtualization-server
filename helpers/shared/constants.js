module.exports = Object.freeze({
  API_ENDPOINT: "http://localhost:8090/",
  API_VIRTUAL: "sv/v1/virtualizations",
  LOADING: false
});
