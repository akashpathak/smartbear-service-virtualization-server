import React from 'react';

export default class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    return (
      <div className="error-page">
        <header className="error-page__header">
          <img
            className="error-page__header-image"
            src="https://static.tutsplus.com/assets/sad-computer-128aac0432b34e270a8d528fb9e3970b.gif"
            alt="Sad computer"
          />
          <h1 className="error-page__title nolinks">Something went wrong</h1>
        </header>
        <p className="error-page__message">
          {this.props.statusCode
            ? `An error ${this.props.statusCode} occurred on server`
            : 'An error occurred on client'}
        </p>
        <style jsx>
          {`
            .error-page {
              margin: 100px 0 40px;
              text-align: center;
            }

            .error-page__header-image {
              width: 112px;
            }

            .error-page__title {
              font-size: 31px;
            }
          `}
        </style>
      </div>
    );
  }
}
