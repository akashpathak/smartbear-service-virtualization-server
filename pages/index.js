import React from "react";
import axios from "axios";
// Component
import Board from "../components/_board";
import Errorpage from "./_error";
import constants from "../helpers/shared/constants";

// Helper for Data fetching
import RequestService from "../helpers/util";
let Loading = constants.LOADING;

class Home extends React.Component {
  static async getInitialProps() {
    let getVirtualDataObject = await RequestService.getVirtualDataObject()
      .then(response => {
        return response.virtualizationList;
      })
      .catch(error => {
        return error;
      });
    return {
      ...this.props,
      getVirtualDataObject
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      getVirtualDataObject: props.getVirtualDataObject
    };
  }

  // Make a seperate call
  getVirtualDataFunc = async () => {
    try {
      let getVirtualDataObject = await RequestService.getVirtualDataObject();
      return getVirtualDataObject;
    } catch (err) {
      return err;
    }
  };

  // Seperated put call
  isRunningToggle = async (id, runningStatus) => {
    try {
      let toggleRunning = await RequestService.EditVirtualization(
        id,
        runningStatus
      );
      toggleRunning === "OK" &&
        this.getVirtualDataFunc().then(res =>
          this.setState({
            getVirtualDataObject: res.virtualizationList
          })
        );
      return toggleRunning;
    } catch (err) {
      return err;
    }
  };

  render() {
    const { getVirtualDataObject } = this.state;
    if (typeof getVirtualDataObject !== "object") {
      return <Errorpage statusCode={getVirtualDataObject} />;
    }
    console.log(this.state);
    return (
      <div className="list">
        <Board
          getVirtualDataObject={getVirtualDataObject}
          isRunningToggle={this.isRunningToggle}
        />
        <style jsx>{`
          @import url("https://fonts.googleapis.com/css?family=Open+Sans:400,700");
          .list {
            font-family: "Open Sans", sans-serif;
            padding: 50px;
            text-align: center;
            background-color: #ddd;
          }

          .photo {
            display: inline-block;
          }

          .photoLink {
            color: #333;
            verticalalign: middle;
            cursor: pointer;
            background: #eee;
            display: inline-block;
            width: 250px;
            height: 250px;
            line-height: 250px;
            margin: 10px;
            border: 2px solid transparent;
          }

          .photoLink:hover {
            bordercolor: blue;
          }
        `}</style>
      </div>
    );
  }
}

export default Home;
